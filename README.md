# Postitykki - Mass emailer

Send customized email messages to a list of people.

# Features

Planned main features (partly implemented; see TODO):

- Render mail templates and pick attachments from pools or attach directly.
- Generates email messages by taking in CSV tables of personal info, attacment specs, mail body template and variables
- Can output messages to .eml files or send directly via SMTP.
- Can use separate workspace directories for different jobs.
    - Helps avoid mixing unrelated files and personal info etc.


# TODO

- Attachment pools
    - Pick from pool
    - Ensure all picks are unique and no duplicate picks can happen
    - Transaction support. Return picked attachment to pool upon delivery failure
- Email body template rendering
- GUI

