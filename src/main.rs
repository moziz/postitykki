use std::collections::{HashMap, HashSet};
use std::fs;
use std::{error::Error, io};
use lettre::message::{Attachment, SinglePart, MultiPart};
use lettre::message::header::ContentType;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, Transport, SmtpTransport, FileTransport};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct SMTPConfig {
	smtp_host: String,
	smtp_user: String,
	smtp_password: String,
	starttls: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct MailJobSpec {
	message_from: String,
	template: String,
	
	#[serde(alias = "attachments", deserialize_with = "deserialize_attachments")]
	attachments: AttachmentDemand,
}

//struct Attachments(Vec<String>);
//impl<'de> Deserialize<'de> for Attachments {
//	fn deserialize<D>(d: D) -> Result<Self, D::Error> where D: serde::Deserializer<'de>

#[derive(Default, Serialize, Deserialize, Debug)]
struct AttachmentDemand {
	// Directly demanded files
	files: Vec<String>,
	// Pick attachments from these pools
	pools: HashSet<String>,
}

fn deserialize_attachments<'de, D>(d: D) -> Result<AttachmentDemand, D::Error> where D: serde::Deserializer<'de>
{
	let attachments_raw: &str = Deserialize::deserialize(d).unwrap_or_default();
	if attachments_raw.is_empty() {
		return Ok(AttachmentDemand::default());
	}
	
	let mut demand = AttachmentDemand::default();
	
	for attachment_spec in attachments_raw.split(';') {
		let mut spec_parts = attachment_spec.split(':');
		match spec_parts.next().zip(spec_parts.next()) {
			None => {},
			Some((cmd, param)) => {
				match cmd {
					"file" => {
						demand.files.push(param.to_owned());
					},
					"pool" => {
						demand.pools.insert(param.to_owned());
					},
					_ => {}
				}
			}
		}
	}
	
	Ok(demand)
}

#[derive(Deserialize, Debug)]
struct MailattavaRecord {
	#[serde(alias = "name")]
	name: String,

	#[serde(alias = "email")]
	email_address: String,

	#[serde(alias = "attachments", deserialize_with = "deserialize_attachments")]
	attachment_demand: AttachmentDemand,
}

struct PoolInfo {
	count: usize,
}

fn discover_attachment_pools() -> HashMap<String, PoolInfo> {
	let pools = HashMap::<String, PoolInfo>::new();
	for entry in fs::read_dir(".").expect("Failed to read workspace directory") {
		let entry = entry.unwrap();
		let metadata = entry.metadata().unwrap();
		
		if !metadata.is_dir() {
			continue;
		}
		
		println!("dir name: {}", entry.file_name().into_string().unwrap());
	}
	
	return pools;
}

fn read_mailattavat(workspace_path: &std::path::Path) -> Result<Vec<MailattavaRecord>, Box<dyn Error>> {
	let mut out = Vec::<MailattavaRecord>::new();
	
	let mut rdr = csv::Reader::from_path(workspace_path.join("targets.csv").as_path())
	.expect("Targets targets.csv missing");
	
	let mut columns_map = HashMap::<String, usize>::new();
	for (i, rec) in rdr.headers().expect("CSV column headers missing").iter().enumerate() {
		columns_map.insert(rec.to_string(), i);
	}
	
	for result in rdr.deserialize() {
		out.push(result?);
	}
	
	Ok(out)
}

fn get_workspace_path() -> Result<std::path::PathBuf, &'static str> {
	let default_path = std::path::PathBuf::from(".");
	
	let ws_param_start_pos = std::env::args().position(|a| a == "--workspace");
	if !ws_param_start_pos.is_some() {
		return Ok(default_path);
	}

	let ws_path_param_pos = ws_param_start_pos.unwrap() + 1;

	match std::env::args().nth(ws_path_param_pos) {
		Some(p) => Ok(default_path.join(p)), // Note: join will handle relative/absolute paths nicely. If p is absolute, it will completely replace the path.
		None => Err("Invalid workspace path. Usage--workspace /path/to/workspace")
	}
}

fn main() {
	let workspace_path: std::path::PathBuf = get_workspace_path().expect("Invalid workspace path");
	let job_config_path = workspace_path.join("job.toml");
	
	let mail_job_spec: MailJobSpec = toml::from_str(
		fs::read_to_string(job_config_path)
		.expect("No job spec found in workspace")
		.as_str()
	).expect("Failed to parse mail job spec");

	println!("job: {:?}", mail_job_spec);
	
	let pools = discover_attachment_pools();
	
	let mailattavat = read_mailattavat(&workspace_path).expect("Reading target list failed");
	println!("mailattavat: {:?}", mailattavat);
	
	let messages: Vec::<Message> = mailattavat.iter().map(|mailattava: &MailattavaRecord| {
		Message::builder()
		.from(mail_job_spec.message_from.parse().unwrap())
		.to(mailattava.email_address.parse().unwrap())
		.subject("Happy new year")
		.multipart(
			MultiPart::mixed()
			.singlepart(
				SinglePart::plain(String::from("oispa joku templatesysteemi tätä viestiä varten"))
			)
			.singlepart(
				Attachment::new(String::from("wolt_code.txt"))
				.body(
					fs::read("Cargo.toml").expect("Attachment file reading failed"),
					ContentType::parse("application/octet-stream").unwrap()
				)
			)
		)
		.unwrap()
	}).collect();
	
	//	let smtp_config_path = format!("{}/.config/wigfi-mailer/smtp-config.toml", std::env::var("HOME").unwrap());
	let smtp_config_path = std::path::PathBuf::from(std::env::var("HOME").unwrap()).join(".config/wigfi-mailer/smtp-config.toml");
	let smtp_config: SMTPConfig = toml::from_str(
		fs::read_to_string(smtp_config_path)
		.expect("Failed to read SMTP config file")
		.as_str()
	).expect("Failed to parse SMTP config");

	let mailer_file = FileTransport::new(workspace_path.join("mail_out/"));
	let mailer_smtp =	if smtp_config.starttls {
		SmtpTransport::starttls_relay(smtp_config.smtp_host.as_str())
	} else {
		SmtpTransport::relay(smtp_config.smtp_host.as_str())
	}
	.unwrap()
	.credentials(Credentials::new(
		smtp_config.smtp_user.to_owned(),
		smtp_config.smtp_password.to_owned()
	))
	.build();
	
	send_messages(&messages, &mailer_file);
}

fn send_messages(messages: &Vec<Message>, mailer: &impl Transport<Error = impl std::fmt::Debug>)
{
	for message in messages {
		println!("Sending to {:?}...", message.headers().get_raw("To").unwrap_or_default());

		// Send the email
		match mailer.send(&message) {
			Ok(_) => println!("Email sent successfully!"),
			Err(e) => println!("Could not send email {:?}", e),
		}
	}
}
